using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EFCoreSample.Libraries.Data;
using EFCoreSample.Libraries.Domain;
using Microsoft.EntityFrameworkCore;

namespace EFCoreSample.Libraries
{
    public class LibraryRepository : ILibraryRepository
    {
        private readonly LibraryContext _db;

        public LibraryRepository(LibraryContext db)
        {
            _db = db;
        }
        
        public async Task<IEnumerable<Library>> GetAll()
        {
            var dataModels = await _db.Libraries
                .Include(library => library.Books)
                .ToListAsync();
            return dataModels.Select(ToLibraryDomainModel);
        }

        public async Task<Library> Get(Guid id)
        {
            var dataModel = await _db.Libraries.Include(e => e.Books)
                .FirstOrDefaultAsync(entity => entity.Id == id);
            return ToLibraryDomainModel(dataModel);
        }

        public async Task<Library> Add(Library library)
        {
            await _db.Libraries.AddAsync(ToLibraryDataModel(library));
            await _db.SaveChangesAsync();
            return library;
        }

        public async Task<Library> Update(Guid id, Library library)
        {
            var toBeUpdated = await _db.Libraries.Include(e => e.Books)
                .FirstOrDefaultAsync(entity => entity.Id == id);
            toBeUpdated.Name = library.Name;
            toBeUpdated.Address = library.Address;

            await _db.SaveChangesAsync();
            return ToLibraryDomainModel(toBeUpdated);
        }

        public async Task Delete(Guid id)
        {
            var toBeDeleted = await _db.Libraries.Include(e => e.Books)
                .FirstOrDefaultAsync(entity => entity.Id == id);
            _db.Remove(toBeDeleted);
            await _db.SaveChangesAsync();
        }

        private LibraryDataModel ToLibraryDataModel(Library library)
        {
            return new LibraryDataModel
            {
                Id = library.Id,
                Name = library.Name,
                Address = library.Address,
                Books = library.Books?.Select(ToBookDataModel).ToList()
            };
        }

        private Library ToLibraryDomainModel(LibraryDataModel libraryDataModel)
        {
            return new Library(libraryDataModel.Id)
            {
                Name = libraryDataModel.Name,
                Address = libraryDataModel.Address,
                Books = libraryDataModel.Books?.Select(ToBookDomainModel).ToList()
            };
        }

        private BookDataModel ToBookDataModel(Book book)
        {
            return new BookDataModel
            {
                Id = book.Id,
                Title = book.Title,
                AuthorName = book.AuthorName,
                Genre = (int) book.Genre,
                PublishDate = book.PublishDate
            };
        }

        private Book ToBookDomainModel(BookDataModel bookDataModel)
        {
            return new Book(bookDataModel.Id)
            {
                Title = bookDataModel.Title,
                AuthorName = bookDataModel.AuthorName,
                Genre = (Genre) bookDataModel.Genre,
                PublishDate = bookDataModel.PublishDate
            };
        }
    }
}