using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EFCoreSample.Libraries.Data;
using EFCoreSample.Libraries.Domain;
using Microsoft.EntityFrameworkCore;

namespace EFCoreSample.Libraries
{
    public class BookRepository : IBookRepository
    {
        private readonly LibraryContext _db;

        public BookRepository(LibraryContext db)
        {
            _db = db;
        }

        public async Task<IEnumerable<Book>> GetAll()
        {
            return (await _db.Books.ToListAsync())
                .Select(ToBookDomainModel);
        }

        public async Task<Book> Get(Guid id)
        {
            return ToBookDomainModel(await _db.Books.FindAsync(id));
        }

        public async Task<Book> Add(Guid libraryId, Book book)
        {
            var bookDataModel = ToBookDataModel(book);
            _db.Books.Add(bookDataModel);
            
            var library = await _db.Libraries.FindAsync(libraryId);
            library.Books.Add(bookDataModel);

            await _db.SaveChangesAsync();
            return book;
        }

        public async Task<Book> Update(Guid bookId, Book book)
        {
            var toBeUpdated = await _db.Books.FindAsync(bookId);
            toBeUpdated.Title = book.Title;
            toBeUpdated.AuthorName = book.AuthorName;
            toBeUpdated.Genre = (int) book.Genre;
            toBeUpdated.PublishDate = book.PublishDate;

            await _db.SaveChangesAsync();
            return ToBookDomainModel(toBeUpdated);
        }

        public async Task Delete(Guid id)
        {
            var toBeDeleted = await _db.Books.FindAsync(id);
            _db.Books.Remove(toBeDeleted);
            await _db.SaveChangesAsync();
        }

        private BookDataModel ToBookDataModel(Book book)
        {
            return new BookDataModel
            {
                Id = book.Id,
                Title = book.Title,
                AuthorName = book.AuthorName,
                Genre = (int) book.Genre,
                PublishDate = book.PublishDate
            };
        }

        private Book ToBookDomainModel(BookDataModel bookDataModel)
        {
            return new Book(bookDataModel.Id)
            {
                Title = bookDataModel.Title,
                AuthorName = bookDataModel.AuthorName,
                Genre = (Genre) bookDataModel.Genre,
                PublishDate = bookDataModel.PublishDate
            };
        }
    }
}