using System;
using System.ComponentModel.DataAnnotations;
using EFCoreSample.Libraries.Domain;

namespace EFCoreSample.Libraries.Requests
{
    public class BookRequest
    {
        [Required]
        public string Title { get; set; }
        [Required]
        public string AuthorName { get; set; }
        [Required]
        public Genre Genre { get; set; } 
        [Required]
        public DateTime PublishDate { get; set; }
    }
}