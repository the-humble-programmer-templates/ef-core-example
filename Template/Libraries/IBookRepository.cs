using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using EFCoreSample.Libraries.Domain;

namespace EFCoreSample.Libraries
{
    public interface IBookRepository
    {
        Task<IEnumerable<Book>> GetAll();

        Task<Book> Get(Guid id);

        Task<Book> Add(Guid libraryId, Book book);

        Task<Book> Update(Guid bookId, Book book);

        Task Delete(Guid id);
    }
}