using System;

namespace EFCoreSample.Libraries.Domain
{
    public class Book 
    {
        public Book(Guid id)
        {
            Id = id;
        }
        
        public Guid Id { get; }
        public string Title { get; set; }
        public string AuthorName { get; set; }
        public Genre Genre { get; set; }
        public DateTime PublishDate { get; set; }
    }
}