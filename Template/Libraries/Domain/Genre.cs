namespace EFCoreSample.Libraries.Domain
{
    public enum Genre
    {
        Fiction = 0,
        NonFiction = 1
    }
}