using System;
using System.Collections.Generic;

namespace EFCoreSample.Libraries.Domain
{
    public class Library
    {
        public Library(Guid id)
        {
            Id = id;
        }
        public Guid Id { get; }
        public string Name { get; set; }
        public string Address { get; set; }
        public List<Book> Books { get; set; } = new List<Book>();
    }
}