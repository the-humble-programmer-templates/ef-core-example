using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using EFCoreSample.Libraries.Domain;
using EFCoreSample.Libraries.Requests;
using Microsoft.AspNetCore.Mvc;

namespace EFCoreSample.Libraries
{
    [ApiController]
    [Route("[controller]")]
    public class LibraryController : ControllerBase
    {
        private readonly ILibraryRepository _repo;

        public LibraryController(ILibraryRepository repo)
        {
            _repo = repo;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<Library>>> GetAll()
        {
            return Ok(await _repo.GetAll());
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Library>> GetById(Guid id)
        {
            return Ok(await _repo.Get(id));
        }

        [HttpPost]
        public async Task<ActionResult<Library>> Post([FromBody] LibraryRequest request)
        {
            var library = new Library(Guid.NewGuid())
            {
                Name = request.Name,
                Address = request.Address
            };

            return Ok(await _repo.Add(library));
        }

        [HttpPut]
        public async Task<ActionResult<Library>> Put(Guid id, [FromBody] LibraryRequest request)
        {
            var library = new Library(id)
            {
                Name = request.Name,
                Address = request.Address
            };

            return Ok(await _repo.Update(id, library));
        }

        [HttpDelete]
        public async Task<IActionResult> Delete(Guid id)
        {
            await _repo.Delete(id);
            return Ok();
        }
    }
}