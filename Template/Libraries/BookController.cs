using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using EFCoreSample.Libraries.Domain;
using EFCoreSample.Libraries.Requests;
using Microsoft.AspNetCore.Mvc;

namespace EFCoreSample.Libraries
{
    [ApiController]
    [Route("[controller]")]
    public class BookController : ControllerBase
    {
        private readonly IBookRepository _repo;

        public BookController(IBookRepository repo)
        {
            _repo = repo;
        }
        
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Book>>> GetAll()
        {
            return Ok(await _repo.GetAll());
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Book>> GetById(Guid id)
        {
            return Ok(await _repo.Get(id));
        }

        [HttpPost("{libraryId}")]
        public async Task<ActionResult<Library>> Post(Guid libraryId, [FromBody] BookRequest request)
        {
            var book = ToBookDomainModel(Guid.NewGuid(), request);

            return Ok(await _repo.Add(libraryId, book));
        }

        [HttpPut]
        public async Task<ActionResult<Library>> Put(Guid id, [FromBody] BookRequest request)
        {
            var book = ToBookDomainModel(id, request);

            return Ok(await _repo.Update(id, book));
        }


        [HttpDelete]
        public async Task<IActionResult> Delete(Guid id)
        {
            await _repo.Delete(id);
            return Ok();
        }

        private Book ToBookDomainModel(Guid id, BookRequest request)
        {
            return new Book(id)
            {
                Title = request.Title,
                AuthorName = request.AuthorName,
                Genre = request.Genre,
                PublishDate = request.PublishDate
            };
        }

    }
}