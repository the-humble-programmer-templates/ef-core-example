using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using EFCoreSample.Libraries.Domain;

namespace EFCoreSample.Libraries
{
    public interface ILibraryRepository
    {
       Task<IEnumerable<Library>> GetAll();

        Task<Library> Get(Guid id);

        Task<Library> Add(Library library);

        Task<Library> Update(Guid id, Library library);

        Task Delete(Guid id);
    }
}