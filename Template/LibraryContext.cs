using EFCoreSample.Libraries.Data;
using Microsoft.EntityFrameworkCore;

namespace EFCoreSample
{
    public class LibraryContext : DbContext
    {
        public LibraryContext(DbContextOptions<LibraryContext> options) : base(options)
        { }

        public DbSet<LibraryDataModel> Libraries { get; set; }
        public DbSet<BookDataModel> Books { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<LibraryDataModel>()
                .HasMany(e => e.Books)
                .WithOne()
                .IsRequired();
        }
    }
}